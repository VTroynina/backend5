<!DOCTYPE html> <html lang="ru"> <head>
  <link rel="stylesheet"
			href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<script
			src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="style.css">
    <title> Задание 1 </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width= device-width, initial-scale=1.0">
  </head>
  <body>
    <div style="height: 70px; text-align: center; background: #BC8F9D; color: #fff; width: 100%">
    <div class="header container">
      <div id="picture"><h1 style="margin: 0 auto;"> <img src="https://www.pinclipart.com/picdir/big/203-2031252_peanuts-box-clip-art-peanuts-free-engine-image.png" style="margin-right: 5px;" width = "45" alt="Кусь"></h1></div>
      <div><h1 id="site"> Let's hunt begin! </h1></div>
    </div>
    </div>
    <div class="px-0 px-sm-3 container">
    <nav id="menu" class="container px-0 mb-3">
       <div class="col-12 my-0 my-md-2 mx-1 py-0 py-md-2 col-md"><a href="#links"> Не </a></div>
       <div class="col-12 my-0 my-md-2 mx-1 py-0 py-md-2 col-md"><a href="#form"> трогай </a> </div>
       <div class="col-12 my-0 my-md-2 mx-1 py-0 py-md-2 col-md"><a href="#table2"> свечку </a> </div>
    </nav>
</div>
    <?php 
  if(!empty($response_message)) {
    print "<div>";
    print $response_message;
    print "</div>";
  }
?>
<?php if(empty($_SESSION['login']))
      print "<div style ='text-align: center; '><h6>Вы можете <a id=\"buttonLogIn\" href='login.php'>войти</a> с логином и паролем для изменения данных</h6></div>" ?>
    <?php if(!empty($_SESSION['login'])) 
      print "<div style ='text-align: center;'><h6><a id=\"buttonLogIn\" href='login.php'>Выйти из аккаунта</a></h6></div>" ?>

  <div class="form container mt-3 mb-0">
    <h1 class="linkformtable" style="padding: 7px;" >Заполните форму для нас: </h1>
    <form class="pl-3" action="."
    method="POST">

    <label>
      Имя:<br />
      <input name="name"
	   <?php if($_COOKIE['name_error']) print "class=error" ?>
        value='<?php print "{$name_field_value}" ?>'/>
    </label><br />

    <label>
      Email:<br />
      <input name="email"
	  <?php if($_COOKIE['name_error']) print "class=error" ?>
        value='<?php print "{$email_field_value}" ?>'
        type="email" />
    </label><br />

    <label>
      Дата рождения:<br />
      <input name="birthday"
	  <?php if($_COOKIE['name_error']) print "class=error" ?>
        value='<?php print "{$bd_field_value}" ?>'
        type="date" />
    </label><br />
    <label>
      Биография:<br />
      <textarea name="biography"><?php print "{$biography_field_value}" ?></textarea>
    </label><br />

      Количество ваших конечностей: <br />
      <label>
        <input type="radio" checked="checked"
        name="limbs" value="1" 
		 <?php if($_COOKIE['limb_number_last_value'] == 1) print "checked" ?>/>
        От 0 до 5
      </label>
      <label>
        <input type="radio"
        name="limbs" value="2" 
		 <?php if($_COOKIE['limb_number_last_value'] == 2) print "checked" ?>/>
        От 6 до 10
      </label>
      <label>
        <input type="radio"
		  <?php if(is_null($_COOKIE['limb_number_last_value']) or !empty($_COOKIE['limb_number_error'])) print "checked" ?> name="limbs" value="3"  
              <?php if($_COOKIE['limb_number_last_value'] == 3) print "checked" ?>
/>
          11 и более
        </label>
        <br />

      Укажите ваши сверхспособности:
      <br />
      <select name="abilitiess[]"
        multiple="multiple">
        <option value="1" <?php if(!empty($_COOKIE['sp1_last_value'] && $_COOKIE['sp1_last_value'])) print "selected" ?>>Управление временем</option>
        <option value="2" <?php if(!empty($_COOKIE['sp2_last_value'] && $_COOKIE['sp2_last_value'])) print "selected" ?>>Паучье чутье</option>
        <option value="3" <?php if(!empty($_COOKIE['sp3_last_value'] && $_COOKIE['sp3_last_value'])) print "selected" ?>>Регенерация</option>
        <option value="4" <?php if(!empty($_COOKIE['sp4_last_value'] && $_COOKIE['sp4_last_value'])) print "selected" ?>>Владение магией</option>
      </select>
    <br />

    Ваш пол:<br />
    <label><input type="radio" <?php if(is_null($_COOKIE['gender_last_value']) or !empty($_COOKIE['gender_error'])) print "checked" ?> name="gender" value="M" 
              <?php if($_COOKIE['gender_last_value'] == 'M') print "checked" ?> />
      Муж</label>
    <label><input type="radio"
      name="gender" value="F" 
	  <?php if($_COOKIE['gender_last_value'] == 'F') print "checked" ?>/>
      Жен</label><br />

    <br />
    <p> <label><input type="checkbox" checked="checked"
      name="contract" <?php if($_COOKIE['contract_error']) print "class=error" ?>/>
      С контрактом ознакомлен</label></p>

    <input type="submit" value="Отправить" />
  </form>
</div>
    </div>
    </div>
<footer>
  <b>(c) For the Horde</b>
</footer>
  </body>
</html>
