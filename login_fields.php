<!DOCTYPE html> <html lang="ru"> <head>
  <link rel="stylesheet"
			href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<script
			src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="style.css">
    <title> Задание 1 </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width= device-width, initial-scale=1.0">
    <style>
   body{
     align-items: center;
   }
   #menu {
    flex-direction: row;
  }
  #main {
    height: 474px;
  }
  .header{
    display: flex;
  }
  #site{
    font-size: 40px;
    margin-left: 300px;
  }
  footer{
    text-align: left;
    letter-spacing: 2px;
    font-size: 18px;
    align-self: stretch;
  }
  footer b {
    padding-left: 20px;
  }
  #login_form {
    text-align: center;
    background-color: #DB706E	;
    border-radius: 10px;
    padding-bottom: 20px;
    padding-top: 85px;
    height: 100%;
  }
  #picture {
    margin-left: 180px;
  }
    input[name="login"], input[name="password"] {
      border-radius: 5px;
      line-height: 1.2;
      font-weight: 500;
      font-size: 14px;
      padding: 26px 24px;
      height: 54px;
      border: 2px solid rgba(0,0,0,0.5);
      margin-top: 5px;
      margin-bottom: 30px;
    }

    input[name="login"]:focus, input[name="password"]:focus {
      border-color: #354e71;
      outline: 0;
      transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    }

    input[type="submit"] {
      padding: 10px;
      width: 100px;
      border-radius: 5px;
    }
    </style>
  </head>
  <body>
    <div style="background: #BC8F9D; color: #fff;" class="header container-fluid">
      <div id="picture"><h1 style="margin: 0 auto;"> <img src="https://www.pinclipart.com/picdir/big/203-2031252_peanuts-box-clip-art-peanuts-free-engine-image.png" style="margin-right: 5px;" width = "45" alt="Кусь"></h1></div>
      <div><h1 id="site"> Let's hunt begin! </h1></div>
    </div>
    <div class="px-0 px-sm-3 container">
    <nav id="menu" class="container px-0 mb-3">
       <div class="col-12 my-0 my-md-2 mx-1 py-0 py-md-2 col-md"><a href="#links"> Не </a></div>
       <div class="col-12 my-0 my-md-2 mx-1 py-0 py-md-2 col-md"><a href="#form"> трогай </a> </div>
       <div class="col-12 my-0 my-md-2 mx-1 py-0 py-md-2 col-md"><a href="#table2"> свечку </a> </div>
    </nav>
</div>
    <?php 
  if(!empty($response_message)) {
    print "<div>";
    print $response_message;
    print "</div>";
  }
?>
    <?php if(!empty($_SESSION['login'])) 
      print "<div style ='text-align: center;'><h6><a id=\"buttonLogIn\" href='login.php'>Выйти из аккаунта</a></h6></div>" ?>
<div class="container my-3 px-4" id="main">
      <form action="" method="post" id="login_form">
      Ваш логин: <br />
        <input name="login" placeholder="Введите логин"/><br />
        Ваш пароль: <br />
        <input name="password" placeholder="Введите пароль" /><br />
        <input type="submit" value="Войти" />
      </form>
    </div>

<footer >
 <b>(c) For the Horde</b>
</footer>
  </body>
</html>
